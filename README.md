# language_school 
    
# description 
    school project 
    subject: KIKM-PPRO UHK
    topic: Language school - courses - entry test 
# participants
    Kvapil Martin (F)
    martin.q.kvapil@gmail.com
    Soldán František (B)
    frantisek.soldan@email.cz
#project_solution
    Cílem aplikace je vyučujícím cizích jazyků vytvářet kurzy, ve kterých mohou mít své testy, které účastníci kurzů vyplňují. 
    Samotní vyučující mohou vytvářet libovolný kurz v jakémkoliv předem definovaném jazyce. Ve svých kurzech mohou přidávat, odebírat nebo upravovat své testy a odpovědi v nich. Uživatelé se mohou do kurzů přihlašovat nebo z nich odhlašovat, vyplňovat kvízy a sledovat úspěšnosti ve svých kurzech. 
#project_artitecture
    MVC
#project_usecase
    